package pieces;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Empty extends Piece{

	/**
	 * @param color
	 */
	public Empty(Color color) {
		super(color);
	}

}
