/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

//import java.util.*;
/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public abstract class Piece {
	
	public enum Color{WHITE, BLACK, EMPTY};
	protected enum Name {PAWN, ROOK, BISHOP, KNIGHT, QUEEN, KING};
	
	protected Color color;
	protected Name name;
	protected boolean moved;
	
	public Piece(Color color) {
		this.color = color;
		this.moved = false;
	}
	
	/**
	 * isValidMove()
	 *
	 * checks if its a valid move for a specific piece
	 *
	 * @param start
	 * @param end
	 * @param board
	 * @return boolean
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		return false;
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 *
	 */
	public String getASCII() {
		if(this.color.equals(Color.WHITE)) {
			return "w";
		} else {
			return "b";
		}
	}
	
	/**
	 * getColor()
	 *
	 *
	 * @return Color
	 *
	 */
	public Color getColor() {
		return this.color;
	}
	
	
	/**
	 * pathClear()
	 *
	 * this checks if the path is clear for long movements,
	 * if there is a piece between start and end then it will return false
	 *
	 * @param start
	 * @param end
	 * @param board
	 * @return boolean
	 *
	 */
	public boolean pathClear(Coord start, Coord end, Board board) {
		int dx = end.getCol() - start.getCol();
		int dy = end.getRow() - start.getRow();
		
		if(dx > 0) {
			dx = 1;
		} else if(dx < 0) {
			dx = -1;
		}
		
		if(dy > 0) {
			dy = 1;
		} else if(dy < 0) {
			dy = -1;
		}
		
		Coord tempCoord = start;
		tempCoord = new Coord(tempCoord.getRow() + dy, tempCoord.getCol() + dx);
		while(!tempCoord.equals(end)) {
			if(!(board.get(tempCoord) instanceof Empty)) {
				return false;
			}
			tempCoord = new Coord(tempCoord.getRow() + dy, tempCoord.getCol() + dx);
		}
		
		return true;
	}
	
	/**
	 * basicMoveValidity()
	 *
	 * checks if the move is valid 
	 * checks if the start location != endLocation
	 * also checks if the end location isn't occupied by the same color
 	 *
	 * @param start
	 * @param end
	 * @param board
	 * @return boolean
	 *
	 */
	public boolean basicMoveValidity(Coord start, Coord end, Board board) {
		// can't be same square
		if(start.equals(end)) {
			return false;
		}
		
		// can't be occupied by own color
		if(board.get(end).getColor().equals(this.getColor())) {
			return false;
		}
		return true;
	}
	
	/**
	 * hasMoved()
	 *
	 * simple check to see if it has Moved
	 *
	 * @return boolean
	 *
	 */
	public boolean hasMoved() {
		return moved;
	}
	
	/**
	 * setMoved()
	 *
	 * updates moved boolean
	 *
	 * @param moved
	 * @return void
	 *
	 */
	public void setMoved(boolean moved) {
		this.moved = moved;
	}
}
