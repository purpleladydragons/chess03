/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Queen extends Piece{
	
	/**
	 * @param color
	 */
	public Queen(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "Q";
	}
	
	/**
	 * isValidMove()
	 *
	 * checks if it is a valid move for a Queen
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		
		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
		
		// can be on diagonal, file, or rank
		if(!(Math.abs(start.getRow() - end.getRow()) == 
				Math.abs(start.getCol() - end.getCol()))) {
			
			if(!(start.getRow() == end.getRow())) {
				if(!(start.getCol() == end.getCol())) {
					return false;
				}
			}
		}
		
		// can't be blocked
		if(!pathClear(start, end, board)) {
			return false;
		}
		
		return true;
	}
}
