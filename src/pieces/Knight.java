/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Knight extends Piece{
	
	/**
	 * @param color
	 */
	public Knight(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "N";
	}
	
	/**
	 * isValidMove()
	 *
	 * checks if the move is valid for a Knight 
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		
		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
		
		// has to be L shape rotated and/or reflected
		if(Math.abs(start.getCol() - end.getCol()) == 1 && 
				Math.abs(start.getRow() - end.getRow()) == 2){
			return true;
		}else if(Math.abs(start.getCol() - end.getCol()) == 2 && 
				Math.abs(start.getRow() - end.getRow()) == 1){
			return true;
		}
		
		return false;
	}
}
