/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Rook extends Piece{
	
	/**
	 * @param color
	 */
	public Rook(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "R";
	}
	
	/**
	 * isValidMove()
	 * 
	 * checks if it is a valid move for a rook
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		
		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
		
		if(!pathClear(start, end, board)) {
			return false;
		} else {
			// can move up/down rank or file
			if(start.getRow() == end.getRow() || 
					start.getCol() == end.getCol()) {
				return true;
			}
		}
		
		return false;
	}
	
	
	
}
