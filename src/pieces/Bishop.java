/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Bishop extends Piece{
	
	/**
	 * @param color
	 */
	public Bishop(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "B";
	}
	
	
	/**
	 * isValidMove()
	 *
	 * checks if its a valid move for bishop
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		
		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
		
		// has to be on diagonal
		if(Math.abs(start.getRow() - end.getRow()) != 
				Math.abs(start.getCol() - end.getCol())) {
			return false;
		}
		
		// can't be blocked
		if(!pathClear(start, end, board)) {
			return false;
		}
		
		return true;
	}
}
