/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class King extends Piece{
	
	/**
	 * @param color
	 */
	public King(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "K";
	}
	
	/**
	 * isValidMove()
	 *
	 * checks if its a valid move for King (immediate proximity)
	 * checks for castling
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		
		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
	
		// can move anywhere in immediate proximity
		if((Math.abs(start.getRow() - end.getRow()) == 1) &&
				(Math.abs(start.getCol() - end.getCol()) == 1)) {
			return true;
		}
		
		// kingside castle
		if(end.getCol() - start.getCol() == 2) {
			if(end.getRow() == start.getRow()) {
				if(!moved) {
					Coord rookCoord = new Coord(end.getRow(), end.getCol() + 1);
					if(board.get(rookCoord) instanceof Rook) {
						Rook rook = (Rook) board.get(rookCoord);
						if(rook.getColor().equals(this.getColor())) {
							if(!rook.hasMoved()) {
								if(pathClear(start, end, board)) {
									// check for castling out of and through check
									if(this.getColor().equals(Color.WHITE)) {
										Board tentativeBoard = board.clone();
										Coord inbetweenCoord = new Coord(start.getRow(), start.getCol()+1);
										tentativeBoard.set(inbetweenCoord, this);
										if(board.isWhiteInCheck() || tentativeBoard.isWhiteInCheck()) {
											return false;
										}
									} else {
										Board tentativeBoard = board.clone();
										Coord inbetweenCoord = new Coord(start.getRow(), start.getCol()+1);
										tentativeBoard.set(inbetweenCoord, this);
										if(board.isBlackInCheck() || tentativeBoard.isBlackInCheck()) {
											return false;
										}
									}
									// end square has to be empty
									if(board.get(end).getColor().equals(Color.EMPTY)) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		
		// queenside castle
		// valid when path to end is clear and
		//	neither king nor rook has moved
		if(start.getCol() - end.getCol() == 2) {
			if(end.getRow() == start.getRow()) {
				if(!moved) {
					Coord rookCoord = new Coord(end.getRow(), end.getCol() - 2);
					if(board.get(rookCoord) instanceof Rook) {
						Rook rook = (Rook) board.get(rookCoord);
						if(rook.getColor().equals(this.getColor())) {
							if(!rook.hasMoved()) {
								if(pathClear(start, end, board)) {
									// check for castling out of and through check
									if(this.getColor().equals(Color.WHITE)) {
										Board tentativeBoard = board.clone();
										Coord inbetweenCoord = new Coord(start.getRow(), start.getCol()-1);
										tentativeBoard.set(inbetweenCoord, this);
										if(board.isWhiteInCheck() || tentativeBoard.isWhiteInCheck()) {
											return false;
										}
									} else {
										Board tentativeBoard = board.clone();
										Coord inbetweenCoord = new Coord(start.getRow(), start.getCol()-1);
										tentativeBoard.set(inbetweenCoord, this);
										if(board.isBlackInCheck() || tentativeBoard.isBlackInCheck()) {
											return false;
										}
									}
									// end square has to be empty
									if(board.get(end).getColor().equals(Color.EMPTY)) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		
		return false;
	}
}
