/**
 * 
 */
package pieces;

import board.Board;
import board.Coord;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Pawn extends Piece{
	
	/**
	 * @param color
	 */
	public Pawn(Color color) {
		super(color);
	}
	
	/**
	 * getASCII()
	 *
	 *
	 * @return String
	 * @see pieces.Piece#getASCII()
	 *
	 */
	public String getASCII() {
		return super.getASCII() + "p";
	}
	
	/**
	 * isValidMove()
	 *
	 * checks if the move is valid for a pawn
	 *
	 * @param start
	 * @param end
	 * @param board
	 * 
	 * @see pieces.Piece#isValidMove(board.Coord, board.Coord, board.Board)
	 *
	 */
	public boolean isValidMove(Coord start, Coord end, Board board){
		

		if(!basicMoveValidity(start, end, board)) {
			return false;
		}
		
		if(this.color.equals(Color.WHITE)) {
			// can move 1-2 squares on start
			if(start.getRow() == 1) {
				if(start.getCol() == end.getCol() &&
						end.getRow() - start.getRow() <= 2 &&
						end.getRow() - start.getRow() > 0 &&
						pathClear(start, end, board)) {
					if(board.get(end) instanceof Empty) {
						return true;
					}
				}
			} 
			
			// can move 1 square forward
			if(start.getCol() == end.getCol() &&
					end.getRow() - start.getRow() == 1) {
				if(board.get(end) instanceof Empty) {
					return true;
				}
			}
			
			// can attack diagonally
			if(Math.abs(start.getCol() - end.getCol()) == 1) {
				if(end.getRow() - start.getRow() == 1) {
					if(board.get(end).getColor().equals(Color.BLACK)) {
						return true;
					}
				}
			}

			// can en passant
			if(start.getRow() == 4) {
				if(Math.abs(start.getCol() - end.getCol()) == 1) {
					if(end.getRow() - start.getRow() == 1) {
						Coord epCoord = new Coord(end.getRow()-1, end.getCol());
						if(board.get(epCoord).getColor().equals(Color.BLACK)) {
							if(board.get(epCoord) instanceof Pawn) {
								if(board.lastMoved().equals(epCoord)) {
									return true;
								}
							}
						}
					}
				}
			}
			
			return false;
		}
		
		if(this.color.equals(Color.BLACK)) {
			// can move 1-2 squares on start
			if(start.getRow() == 6) {
				if(start.getCol() == end.getCol() &&
						start.getRow() - end.getRow() <= 2 &&
						start.getRow() - end.getRow() > 0 &&
						pathClear(start, end, board)) {
					if(board.get(end) instanceof Empty) {
						return true;
					}
				}
			} 
			
			// can move 1 square forward
			if(start.getCol() == end.getCol() &&
					start.getRow() - end.getRow() == 1) {
				if(board.get(end) instanceof Empty) {
					return true;
				}
			}
			
			// can attack diagonally
			if(Math.abs(start.getCol() - end.getCol()) == 1) {
				if(start.getRow() - end.getRow() == 1) {
					if(board.get(end).getColor().equals(Color.WHITE)) {
						return true;
					}
				}
			}
			
			// can en passant
			if(start.getRow() == 3) {
				if(Math.abs(start.getCol() - end.getCol()) == 1) {
					if(start.getRow() - end.getRow() == 1) {
						Coord epCoord = new Coord(end.getRow()+1, end.getCol());
						if(board.get(epCoord).getColor().equals(Color.WHITE)) {
							if(board.get(epCoord) instanceof Pawn) {
								if(board.lastMoved().equals(board.get(epCoord))) {
									return true;
								} 
							}
						}
					}
				}
			}
			
			return false;
		}
		
		return false;
		
	}
}
