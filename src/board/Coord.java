package board;

/**
 * @author Vignesh Patel & Austin Steady
 *
 */
public class Coord {
	int row;
	int col;
	
	public Coord(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	/**
	 * getRow()
	 *
	 * returns row 
	 *
	 * @return int
	 *
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * getCol()
	 *
	 * returns column
	 * 
	 * @return int
	 *
	 */
	public int getCol() {
		return col;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coord other = (Coord) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
}