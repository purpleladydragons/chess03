package board;

import main.Driver;
import pieces.Bishop;
import pieces.Empty;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Piece.Color;
import pieces.Queen;
import pieces.Rook;

/**
 * 
 * @author Austin Steady & Vignesh Patel
 *
 */

public class Board {

	public Piece[][] grid;
	Coord whiteKingCoord;
	Coord blackKingCoord;
	Coord lastMoved;
	
	public Board() {
		lastMoved = new Coord(-1, -1);
		
		grid = new Piece[8][8];
		
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				grid[i][j] = new Empty(Color.EMPTY);
			}
		}
		
		grid[0][0] = new Rook(Piece.Color.WHITE);
		grid[0][1] = new Knight(Piece.Color.WHITE);
		grid[0][2] = new Bishop(Piece.Color.WHITE);
		grid[0][3] = new Queen(Piece.Color.WHITE);
		grid[0][4] = new King(Piece.Color.WHITE);
		grid[0][5] = new Bishop(Piece.Color.WHITE);
		grid[0][6] = new Knight(Piece.Color.WHITE);
		grid[0][7] = new Rook(Piece.Color.WHITE);
		
		grid[1][0] = new Pawn(Piece.Color.WHITE);
		grid[1][1] = new Pawn(Piece.Color.WHITE);
		grid[1][2] = new Pawn(Piece.Color.WHITE);
		grid[1][3] = new Pawn(Piece.Color.WHITE);
		grid[1][4] = new Pawn(Piece.Color.WHITE);
		grid[1][5] = new Pawn(Piece.Color.WHITE);
		grid[1][6] = new Pawn(Piece.Color.WHITE);
		grid[1][7] = new Pawn(Piece.Color.WHITE);
		
		
		grid[7][0] = new Rook(Piece.Color.BLACK);
		grid[7][1] = new Knight(Piece.Color.BLACK);
		grid[7][2] = new Bishop(Piece.Color.BLACK);
		grid[7][3] = new Queen(Piece.Color.BLACK);
		grid[7][4] = new King(Piece.Color.BLACK);
		grid[7][5] = new Bishop(Piece.Color.BLACK);
		grid[7][6] = new Knight(Piece.Color.BLACK);
		grid[7][7] = new Rook(Piece.Color.BLACK);
		
		grid[6][0] = new Pawn(Piece.Color.BLACK);
		grid[6][1] = new Pawn(Piece.Color.BLACK);
		grid[6][2] = new Pawn(Piece.Color.BLACK);
		grid[6][3] = new Pawn(Piece.Color.BLACK);
		grid[6][4] = new Pawn(Piece.Color.BLACK);
		grid[6][5] = new Pawn(Piece.Color.BLACK);
		grid[6][6] = new Pawn(Piece.Color.BLACK);
		grid[6][7] = new Pawn(Piece.Color.BLACK);
		
		whiteKingCoord = new Coord(0, 4);
		blackKingCoord = new Coord(7, 4);
	}
	
	/**
	 * printBoard()
	 * 
	 * prints the board every time this method is called
	 *
	 * @return void
	 *
	 */
	public void printBoard() {
		
		for(int row=grid.length-1;row>=0;row--) {
			for(int col=0;col<grid[0].length;col++) {
				if(!(grid[row][col] instanceof Empty)) {
					System.out.print(grid[row][col].getASCII());
				} else {
					if( (row + col) % 2 == 0) {
						System.out.print("##");
					} else {
						System.out.print("  ");
					}
				}
				
				System.out.print(" ");
			}
			
			// print row label
			System.out.print(row+1);
			// print next row
			System.out.print("\n");
		}
		
		// print cols
		for(char ch : "abcdefg".toCharArray() ) {
			System.out.print(" " + ch + " ");
		}
		// print last col
		System.out.print(" h");
		
		System.out.print("\n\n");
	}
	
	/**
	 * isBlackStuck()
	 *
	 * checks if the black user can make any valid moves without resulting in check
	 *
	 * @return boolean
	 *
	 */
	public boolean isBlackStuck() {
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				Piece p = grid[i][j];
				if(!p.getColor().equals(Color.BLACK)) {
					continue;
				}
				Board tentativeBoard = this.clone();
				Coord start = new Coord(i,j);
				for(int x=0;x<grid.length;x++) {
					for(int y=0;y<grid[0].length;y++) {
						Coord end = new Coord(x, y);
						if(p.isValidMove(start, end, this)) {
							Driver.executeMove(start, end, tentativeBoard, "");
							if(!tentativeBoard.isBlackInCheck()) {
								return false;
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * isWhiteStuck()
	 *
	 * checks if the white user can make any valid moves without resulting in check
	 *
	 * @return boolean
	 *
	 */
	public boolean isWhiteStuck() {
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				Piece p = grid[i][j];
				if(!p.getColor().equals(Color.WHITE)) {
					continue;
				}
				Board tentativeBoard = this.clone();
				Coord start = new Coord(i,j);
				for(int x=0;x<grid.length;x++) {
					for(int y=0;y<grid[0].length;y++) {
						Coord end = new Coord(x, y);
						if(p.isValidMove(start, end, this)) {
							Driver.executeMove(start, end, tentativeBoard, "");
							if(!tentativeBoard.isWhiteInCheck()) {
								return false;
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * lastMoved()
	 *
	 * 
	 * @return Coord
	 *
	 */
	public Coord lastMoved() {
		return lastMoved;
	}
	
	/**
	 * isStaleMate()
	 *
	 * checks if it is stalemate
	 *
	 * @return boolean
	 *
	 */
	public boolean isStaleMate() {
		return false;
	}
	
	/**
	 * isWhiteInCheck()
	 *
	 * checks if the white user is in check
	 *
	 * @return boolean
	 *
	 */
	public boolean isWhiteInCheck() {
		for(int row=0;row<8;row++) {
			for(int col=0;col<8;col++) {
				Piece piece = grid[row][col];
				if(piece.getColor().equals(Color.BLACK)) {
					if(piece.isValidMove(new Coord(row, col), whiteKingCoord, this)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * isBlackInCheck()
	 *
	 * checks if the black user is in check
	 *
	 * @return boolean
	 *
	 */
	public boolean isBlackInCheck() {
		for(int row=0;row<8;row++) {
			for(int col=0;col<8;col++) {
				Piece piece = grid[row][col];
				if(piece.getColor().equals(Color.WHITE)) {
					if(piece.isValidMove(new Coord(row, col), blackKingCoord, this)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * notationToCoordinate()
	 *
	 * converts notations to coordinates
	 * 
	 * @param square
	 * @return Coord
	 *
	 */
	public Coord notationToCoordinate(String square) {
		
		int col = "abcdefgh".indexOf(square.charAt(0));
		int row = Integer.parseInt("" + square.charAt(1), 10);
		
		Coord coords = new Coord(row-1, col);
		return coords;
	}
	
	/**
	 * validNotation()
	 *
	 * checks if the input notation is valid
	 *
	 * @param square
	 * @return boolean
	 *
	 */
	public boolean validNotation(String square) {
		if(square.length() == 2) {
			int col = "abcdefgh".indexOf(square.charAt(0));
			try {
				int rank = Integer.parseInt("" + square.charAt(1), 10);
				// col 0 to 7 and rank 1 to 8
				if(col > -1 && col < 8 && rank >= 1 && rank <= 8) {
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}
		
		return false;
	}
	
	/**
	 * get()
	 *
	 * returns the piece on that specific coordinate
	 *
	 * @param coord
	 * @return Piece
	 *
	 */
	public Piece get(Coord coord) {
		return grid[coord.getRow()][coord.getCol()];
	}
	
	/**
	 * set()
	 *
	 * Sets king coordinates
	 *
	 * @param coord
	 * @param piece
	 * @return void
	 *
	 */
	public void set(Coord coord, Piece piece) {
		grid[coord.getRow()][coord.getCol()] = piece;
		if(piece instanceof King) {
			if(piece.getColor().equals(Color.WHITE)) {
				whiteKingCoord = coord;
			} else {
				blackKingCoord = coord;
			}
		}
		
		// record last moved piece for en passant purposes
		if(!(piece instanceof Empty)) {
			lastMoved = coord;
		}
	}
	
	@Override
	public Board clone() {
		Board board = new Board();
		for(int i=0;i<grid.length;i++) {
			board.grid[i] = grid[i].clone();
		}
		board.blackKingCoord = this.blackKingCoord;
		board.whiteKingCoord = this.whiteKingCoord;
		
		return board;
	}
}
