package main;

import java.util.Scanner;

import pieces.Bishop;
import pieces.Empty;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Piece.Color;
import pieces.Queen;
import pieces.Rook;
import board.Board;
import board.Coord;

/**
 * 
 * @author Austin Steady & Vignesh Patel
 *
 */

public class Driver {

	static Board board;
	static boolean drawOffered;
	static int turnsMade;
	static boolean tentativelyValid;
	
	static final String CHECK_STRING = "Check\n";
	static final String STALEMATE_STRING = "Stalemate\n";
	static final String CHECKMATE_STRING = "Checkmate\n";
	static final String ILLEGAL_MOVE_STRING = "Illegal move, try again\n";
	static final String WHITE_WINS_STRING = "White wins";
	static final String BLACK_WINS_STRING = "Black wins";
	static final String GAME_DRAW_STRING = "Draw";
	
	static final String RESIGN_STRING = "resign";
	static final String DRAW_STRING = "draw";
	static final String DRAW_REQUEST_STRING = "draw?";
	static final String PROMOTION_PIECES = "NBRQ";
	
	
	public static void main(String[] args) {
		
		board = new Board();
		turnsMade = 0;
		drawOffered = false;
		tentativelyValid = false;
		
		while(true) {
			while(!userTurnLoop());
			turnsMade++;
		}
	}
	
	/**
	 * executeMove()
	 *
	 * executes the move using starting location, ending location, current state of board and the piece that has to be promoted
	 * this method also handles Castling  
	 *
	 * @param start
	 * @param end
	 * @param tboard
	 * @param promotion
	 * @return void
	 *
	 */
	public static void executeMove(Coord start, Coord end, Board tboard, String promotion) {
		if(tboard.get(start) instanceof King) {
			// kingside castle
			if(end.getCol() - start.getCol() == 2) {
				Coord rookCoord = new Coord(end.getRow(), end.getCol()+1);
				Coord newRookCoord = new Coord(end.getRow(), end.getCol()-1);
				Piece rook = tboard.get(rookCoord);
				tboard.set(newRookCoord, rook);
				tboard.set(rookCoord, new Empty(Color.EMPTY));
			}
			
			// queenside
			else if(start.getCol() - end.getCol() == 2) {
				Coord rookCoord = new Coord(end.getRow(), end.getCol()-2);
				Coord newRookCoord = new Coord(end.getRow(), end.getCol()+1);
				Piece rook = tboard.get(rookCoord);
				rook.setMoved(true);
				tboard.set(newRookCoord, rook);
				tboard.set(rookCoord, new Empty(Color.EMPTY));
			}
		}
		
		if(tboard.get(start) instanceof Pawn) {
			Piece pawn = tboard.get(start);
			// white en passant
			if(pawn.getColor().equals(Color.WHITE)) {
				if(start.getRow() == 4 && start.getCol() != end.getCol()) {
					// this combined with col != othercol is enough to identify en passant
					if(tboard.get(end) instanceof Empty) {
						Coord passedPawnCoord = new Coord(end.getRow()-1, end.getCol());
						tboard.set(passedPawnCoord, new Empty(Color.EMPTY));
					}
				}
			}
			
			// black en passant
			if(pawn.getColor().equals(Color.BLACK)) {
				if(start.getRow() == 3 && start.getCol() != end.getCol()) {
					// this combined with col != othercol is enough to identify en passant
					if(tboard.get(end) instanceof Empty) {
						Coord passedPawnCoord = new Coord(end.getRow()+1, end.getCol());
						tboard.set(passedPawnCoord, new Empty(Color.EMPTY));
					}
				}
			}
		}
		
		if(tboard.get(start) instanceof Pawn) {
			Piece pawn = tboard.get(start);
			Piece promoted = null;
			if(end.getRow() == 7 || end.getRow() == 0) {
				if(promotion.equals("Q")) {
					promoted = new Queen(pawn.getColor());
				} else if(promotion.equals("R")) {
					promoted = new Rook(pawn.getColor());
				} else if(promotion.equals("N")) {
					promoted = new Knight(pawn.getColor());
				} else if(promotion.equals("B")) {
					promoted = new Bishop(pawn.getColor());
				} else {
					promoted = new Queen(pawn.getColor());
				}
				// now promoted piece will be moved to end square
				//	instead of pawn
				tboard.set(start, promoted);
			}
		}
		
		// normal move
		//	move the piece and set its old square to empty
		Piece moving = tboard.get(start);
		moving.setMoved(true);
		tboard.set(end, moving);
		tboard.set(start, new Empty(Color.EMPTY));
		tentativelyValid = true;
	}
	
	/**
	 * promptUser()
	 *
	 * this method gets user input
	 * 
	 * @return String
	 *
	 */
	public static String promptUser() {
		Scanner reader = new Scanner(System.in);
		if(whitesMove()) {
			System.out.print("White's move: ");
		} else {
			System.out.print("Black's move: ");
		}
		
		String entry = reader.nextLine();
		System.out.print("\n");
		return entry;
	}
	
	/**
	 * whitesMove()
	 *
	 * this method simply checks if it is white or black users move
	 * 
	 * @return boolean
	 *
	 */
	public static boolean whitesMove() {
		return turnsMade % 2 == 0;
	}
	
	/**
	 * userTurnLoop()
	 *
	 * this method basically checks for all the cases like draw, win, check, checkmate and normal promotion of a piece
	 * 
	 * @return boolean
	 *
	 */
	public static boolean userTurnLoop() {
		board.printBoard();
		tentativelyValid = false;
		Board tentativeBoard = board.clone();
		String userEntry = promptUser();
		
		String[] tokens = userEntry.split(" ");
		
		if(tokens.length == 1) {
			if(tokens[0].equals(RESIGN_STRING)) {
				if(whitesMove()) {
					System.out.println(BLACK_WINS_STRING);
				} else {
					System.out.println(WHITE_WINS_STRING);
				}
				System.exit(1);
			}
			
			if(tokens[0].equals(DRAW_STRING)) {
				if(drawOffered) {
					System.out.println(GAME_DRAW_STRING);
					System.exit(1);
				}
			}
			
		}
		
		if(tokens.length == 2) {
			String start = tokens[0];
			String end = tokens[1];
			
			if(board.validNotation(start) && board.validNotation(end)) {
				Coord sCoord = board.notationToCoordinate(start);
				Coord eCoord = board.notationToCoordinate(end);
				
				// if NOT (white's move XOR piece is black) 
				// this is validating that piece belongs to player
				if(!(whitesMove() ^ board.get(sCoord).getColor().equals(Color.BLACK))) {
					System.out.println(ILLEGAL_MOVE_STRING);
					return false;
				}
				
				if(board.get(sCoord).isValidMove(sCoord, eCoord, board)) {
					executeMove(sCoord, eCoord, tentativeBoard, "");
					drawOffered = false;
				} else {
					System.out.println(ILLEGAL_MOVE_STRING);
					return false;
				}
			}
		}
		
		if(tokens.length == 3) {
			// draw request
			if(tokens[2].equals(DRAW_REQUEST_STRING)) {
				String start = tokens[0];
				String end = tokens[1];
				if(board.validNotation(start) && board.validNotation(end)) {
					Coord sCoord = board.notationToCoordinate(start);
					Coord eCoord = board.notationToCoordinate(end);
					
					// if NOT (white's move XOR piece is black) 
					// this is validating that piece belongs to player
					if(!(whitesMove() ^ board.get(sCoord).getColor().equals(Color.BLACK))) {
						System.out.println(ILLEGAL_MOVE_STRING);
						return false;
					}
					
					if(board.get(sCoord).isValidMove(sCoord, eCoord, board)) {
						executeMove(sCoord, eCoord, tentativeBoard, "");
						drawOffered = true;
					} else {
						System.out.println(ILLEGAL_MOVE_STRING);
						return false;
					}
				}
			}
			
			// promotion
			if(PROMOTION_PIECES.indexOf(tokens[2]) > -1) {
				String start = tokens[0];
				String end = tokens[1];
				if(board.validNotation(start) && board.validNotation(end)) {
					Coord sCoord = board.notationToCoordinate(start);
					Coord eCoord = board.notationToCoordinate(end);
					
					// if NOT (white's move XOR piece is black) 
					// this is validating that piece belongs to player
					if(!(whitesMove() ^ board.get(sCoord).getColor().equals(Color.BLACK))) {
						System.out.println(ILLEGAL_MOVE_STRING);
						return false;
					}
					
					if(board.get(sCoord) instanceof Pawn) {
						// end ranks
						if(eCoord.getRow() == 7 || eCoord.getRow() == 0) {
							if(board.get(sCoord).isValidMove(sCoord, eCoord, board)) {
								executeMove(sCoord, eCoord, tentativeBoard, tokens[2]);
								drawOffered = false;
							} else {
								System.out.println(ILLEGAL_MOVE_STRING);
								return false;
							}
						}
					}
					
				}
			}
		}
		
		if(tokens.length == 4) {
			if(PROMOTION_PIECES.indexOf(tokens[2]) > -1 && 
					tokens[3].equals(DRAW_REQUEST_STRING)) {
				String start = tokens[0];
				String end = tokens[1];
				if(board.validNotation(start) && board.validNotation(end)) {
					Coord sCoord = board.notationToCoordinate(start);
					Coord eCoord = board.notationToCoordinate(end);
					
					// if NOT (white's move XOR piece is black) 
					// this is validating that piece belongs to player
					if(!(whitesMove() ^ board.get(sCoord).getColor().equals(Color.BLACK))) {
						System.out.println(ILLEGAL_MOVE_STRING);
						return false;
					}
					
					if(board.get(sCoord) instanceof Pawn) {
						// end ranks
						if(eCoord.getRow() == 7 || eCoord.getRow() == 0) {
							if(board.get(sCoord).isValidMove(sCoord, eCoord, board)) {
								executeMove(sCoord, eCoord, tentativeBoard, tokens[2]);
								drawOffered = true;
							} else {
								System.out.println(ILLEGAL_MOVE_STRING);
								return false;
							}
						}
					}
					
				}
			}
		}
		
		if(tentativelyValid) {
			if(tentativeBoard.isWhiteInCheck()) {
				if(whitesMove()) {
					System.out.println(ILLEGAL_MOVE_STRING);
					return false;
				} else {
					board = tentativeBoard;
					if(board.isWhiteStuck()) {
						board.printBoard();
						System.out.println(CHECKMATE_STRING);
						System.out.println(BLACK_WINS_STRING);
						System.exit(1);
					}
					System.out.println(CHECK_STRING);
					return true;
				}
			} 
			
			if(tentativeBoard.isBlackInCheck()) {
				if(!whitesMove()) {
					System.out.println(ILLEGAL_MOVE_STRING);
					return false;
				} else {
					board = tentativeBoard;
					if(board.isBlackStuck()) {
						board.printBoard();
						System.out.println(CHECKMATE_STRING);
						System.out.println(WHITE_WINS_STRING);
						System.exit(1);
					}
					System.out.println(CHECK_STRING);
					return true;
				}
			}
			
			if(!tentativeBoard.isWhiteInCheck()) {
				if(!whitesMove()) {
					if(tentativeBoard.isWhiteStuck()) {
						tentativeBoard.printBoard();
						System.out.println(STALEMATE_STRING);
						System.exit(1);
					}
				}
			}
			
			if(!tentativeBoard.isBlackInCheck()) {
				if(whitesMove()) {
					if(tentativeBoard.isBlackStuck()) {
						tentativeBoard.printBoard();
						System.out.println(STALEMATE_STRING);
						System.exit(1);
					}
				}
			}
			
			board = tentativeBoard;
			return true;
		}
		
		System.out.println(ILLEGAL_MOVE_STRING);
		return false;
	}
}
